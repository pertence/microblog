package com.example.app.utils

fun formatDate(date: String) = date.substring(11, 13) + "h" +
        date.substring(14, 16) + " - " +
        date.substring(8, 10) + "/" +
        date.substring(5, 7) + "/" +
        date.substring(0, 4)