package com.example.app

import android.app.Application
import androidx.room.Room
import com.example.app.data.database.AppDatabase

class MicroBlogApplication : Application() {

    companion object {
        var appDatabase: AppDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        appDatabase = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "app_db"
        ).fallbackToDestructiveMigration().build()
    }

}