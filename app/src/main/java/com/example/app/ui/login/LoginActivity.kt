package com.example.app.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.app.databinding.ActivityLoginBinding
import com.example.app.ui.main.MainActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class LoginActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        mAuth = FirebaseAuth.getInstance()

        val registerLayout = binding.layoutRegister
        val registerUserName = binding.registerUserName
        val registerEmail = binding.registerEmailInput
        val registerPassword = binding.registerPasswordInput
        val registerConfirmPassword = binding.registerConfirmPasswordInput
        val registerButtonOtherAccount = binding.registerAnotherButton
        val registerButtonRegister = binding.registerRegistrarButton

        val loginLayout = binding.layoutLogin
        val loginEmail = binding.loginEmailInput
        val loginPassword = binding.loginPasswordInput
        val loginButtonRegister = binding.loginRegistrarButton
        val loginButtonSignIn = binding.loginEnterButton

        val progressBar = binding.loginProgress

        loginButtonRegister.setOnClickListener {
            registerLayout.visibility = View.VISIBLE
            loginLayout.visibility = View.GONE
        }

        registerButtonOtherAccount.setOnClickListener {
            registerLayout.visibility = View.GONE
            loginLayout.visibility = View.VISIBLE
        }
        registerButtonRegister.setOnClickListener {
            register(
                registerUserName.text.toString(),
                registerEmail.text.toString(),
                registerPassword.text.toString(),
                registerConfirmPassword.text.toString(),
                progressBar
            )
        }
        loginButtonSignIn.setOnClickListener {
            login(
                loginEmail.text.toString(),
                loginPassword.text.toString(),
                progressBar
            )
        }

    }

    private fun register(
        registerName: String,
        loginEmail: String,
        loginPassword: String,
        registerConfirmPassword: String,
        progressBar: ProgressBar
    ) {
        if (
            !TextUtils.isEmpty(loginEmail) &&
            !TextUtils.isEmpty(loginPassword) &&
            !TextUtils.isEmpty(registerConfirmPassword)
        ) {
            if (loginPassword == registerConfirmPassword) {

                progressBar.visibility = View.VISIBLE
                mAuth.createUserWithEmailAndPassword(loginEmail, loginPassword)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mAuth.currentUser?.updateProfile(
                                UserProfileChangeRequest.Builder().setDisplayName(registerName)
                                    .build()
                            )
                            goToHome()
                        } else {
                            val error = task.exception?.message
                            Toast.makeText(
                                this, "Error while registering: $error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        progressBar.visibility = View.GONE
                    }

            } else {
                Toast.makeText(
                    this, "Password must be the same on both fields",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun login(
        loginEmail: String,
        loginPassword: String,
        progressBar: ProgressBar
    ) {
        if (!TextUtils.isEmpty(loginEmail) && !TextUtils.isEmpty(loginPassword)) {
            progressBar.visibility = View.VISIBLE
            mAuth.signInWithEmailAndPassword(loginEmail, loginPassword)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        goToHome()
                    } else {
                        val error = task.exception?.message
                        Toast.makeText(this, "Error while logging: $error", Toast.LENGTH_SHORT)
                            .show()
                        progressBar.visibility = View.GONE
                    }
                }
        }
    }

    private fun goToHome() {
        startActivity(Intent(this, MainActivity::class.java))
    }

}