package com.example.app.ui.detail

import android.app.Activity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.app.R
import com.example.app.data.model.Post
import com.example.app.ui.components.Footer
import com.example.app.ui.home.HomeViewModel
import com.example.app.utils.formatDate
import com.google.firebase.auth.FirebaseAuth
import java.time.LocalDateTime

@Composable
fun DetailScreen(
    post: Post,
    auth: FirebaseAuth,
    homeViewModel: HomeViewModel,
    context: Activity
) {

    val isEditMode = remember {
        mutableStateOf(false)
    }

    Column(
        Modifier
            .background(color = colorResource(id = R.color.white_gray))
            .fillMaxSize()
    ) {
        Card(
            border = BorderStroke(2.dp, Color.Transparent),
            elevation = 18.dp,
            backgroundColor = colorResource(id = R.color.white_gray),
            shape = RoundedCornerShape(16.dp),
            modifier = Modifier
                .padding(6.dp)
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally)
        ) {
            Column(
                modifier = Modifier.padding(6.dp)
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .wrapContentSize(Alignment.Center)

                ) {
                    Image(
                        painter = rememberImagePainter(data = post.user_profile),
                        contentDescription = "Image from ${post.user_name}",
                        modifier = Modifier
                            .size(75.dp)
                            .clip(CircleShape)
                            .border(1.dp, Color.White, CircleShape)
                            .background(
                                color = colorResource(
                                    id = R.color.light_grey
                                )
                            )
                    )
                    Spacer(Modifier.width(10.dp))
                    Text(post.user_name, style = MaterialTheme.typography.h5)
                }

                Spacer(Modifier.height(10.dp))

                if (isEditMode.value) {
                    homeViewModel.onPostTextChanged(post.message_content)
                    val textState: String = homeViewModel.postTextEdit.value
                    val maxChar = 280

                    val query = remember {
                        mutableStateOf(textState)
                    }

                    OutlinedTextField(
                        value = query.value,
                        onValueChange = {
                            query.value = it.take(maxChar)
                            homeViewModel.onPostTextChanged(it.take(maxChar))
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.Transparent)
                    )

                    Spacer(Modifier.height(10.dp))

                    Button(
                        onClick = {
                            post.message_content = query.value
                            post.message_created =
                                "edited at ${formatDate(LocalDateTime.now().toString())}"
                            homeViewModel.updatePostFromDatabase(post)
                            goToMainActivity(context)
                            context.finish()
                        },
                        shape = RoundedCornerShape(30.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color.Black,
                            contentColor = Color.White
                        ),
                        modifier = Modifier
                            .align(Alignment.End)
                            .padding(8.dp)
                            .width(90.dp)
                            .height(35.dp)
                    ) {
                        Text(text = "Edit", fontWeight = FontWeight.Bold)
                    }
                } else {
                    SelectionContainer {
                        Column {
                            Text(
                                post.message_content,
                                style = MaterialTheme.typography.subtitle1,
                                fontSize = 20.sp
                            )
                        }
                    }
                }
                Spacer(Modifier.height(10.dp))
                Text(
                    post.message_created,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.align(Alignment.End)
                )
            }
        }
        if (auth.currentUser?.displayName == post.user_name) {
            Footer(post, homeViewModel, context, isEditMode)
        }
    }
}