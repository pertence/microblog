package com.example.app.ui.news

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.colorResource
import com.example.app.R
import com.example.app.data.model.Post
import com.example.app.ui.components.ScrollingListNews

@Composable
fun NewsScreen(newsViewModel: NewsViewModel) {
    val owner = LocalLifecycleOwner.current
    val listNews: MutableList<Post> = mutableListOf()
    val list by remember {
        mutableStateOf(listNews)
    }

    newsViewModel.getAllNewsList().observe(owner, { newsList ->
        listNews.addAll(newsList)
        Log.d("NewsScreen: ", listNews.toString())
    })

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.93f)
            .background(colorResource(id = R.color.white))
            .wrapContentSize(Alignment.Center)
    ) {
        ScrollingListNews(list)
    }
}