package com.example.app.ui.components

import com.example.app.R

sealed class NavigationItem(var route: String, var icon: Int, var title: String) {
    object Home : NavigationItem("home", R.drawable.ic_home, "Home")
    object NewPost : NavigationItem("new_post", R.drawable.ic_new, "New Post")
    object News : NavigationItem("news", R.drawable.ic_focus, "News")
}
