package com.example.app.ui.components

import android.app.Activity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.app.data.model.Post
import com.example.app.ui.detail.deleteFromDatabase
import com.example.app.ui.home.HomeViewModel

@Composable
fun Footer(
    post: Post,
    homeViewModel: HomeViewModel,
    context: Activity,
    isEditMode: MutableState<Boolean>
) {

    val openDialog = remember {
        mutableStateOf(false)
    }

    Column(
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.End,
        modifier = Modifier.fillMaxWidth()
    ) {

    }
    Row(
        horizontalArrangement = Arrangement.End,
        verticalAlignment = Alignment.Bottom,
        modifier = Modifier.fillMaxWidth()
    ) {
        IconToggleButton(
            checked = false,
            onCheckedChange = {
                isEditMode.value = !isEditMode.value
            },
            modifier = Modifier.padding(8.dp),
            content = {
                Icon(imageVector = Icons.Default.Edit, contentDescription = "Edit")
            }
        )
        IconButton(
            onClick = {
                openDialog.value = true
            },
            modifier = Modifier.padding(8.dp),
            content = {
                Icon(imageVector = Icons.Default.Delete, contentDescription = "Delete")
            }
        )

    }

    if (openDialog.value) {
        AlertDialog(
            title = {
                Text("Delete post")
            },
            text = { Text("Do you want delete this post?") },
            onDismissRequest = { openDialog.value = false },
            modifier = Modifier
                .fillMaxWidth()
                .border(BorderStroke(2.dp, Color.Transparent))
                .shadow(12.dp),
            confirmButton = {
                TextButton(onClick = {
                    deleteFromDatabase(homeViewModel, post, context)
                    openDialog.value = false
                }) {
                    Text(text = "Delete", color = Color.Black)
                }
            },
            dismissButton = {
                TextButton(onClick = { openDialog.value = false }) {
                    Text(text = "Cancel", color = Color.Black)
                }
            }
        )

    }
}