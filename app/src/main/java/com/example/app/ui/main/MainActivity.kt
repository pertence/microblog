package com.example.app.ui.main

import android.app.Activity
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.app.data.model.Post
import com.example.app.ui.components.BottomNavigationBar
import com.example.app.ui.components.NavigationBottom
import com.example.app.ui.components.TopBar
import com.example.app.ui.home.HomeViewModel
import com.example.app.ui.newpost.NewPostViewModel
import com.example.app.ui.news.NewsViewModel
import com.google.firebase.auth.FirebaseAuth
import java.time.Instant
import java.time.format.DateTimeFormatter

class MainActivity : ComponentActivity() {
    private val exampleLiveData = MutableLiveData("")

    private lateinit var newsViewModel: NewsViewModel
    private lateinit var newPostViewModel: NewPostViewModel
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var context: Activity

    @ExperimentalComposeUiApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val auth = FirebaseAuth.getInstance()

        context = this

        homeViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(HomeViewModel::class.java)

        newsViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(NewsViewModel::class.java)

        newPostViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(NewPostViewModel::class.java)

        homeViewModel.putFakeOnDatabase()
        newsViewModel.getNewsFromApiAndPutInDatabase()

        val listPost: MutableList<Post> = mutableListOf()
        val list = mutableStateOf(listPost)

        homeViewModel.putFakeOnDatabase()
        homeViewModel.getAllPostList().observe(this, { newList ->
            listPost.addAll(newList)
        })

        setContent {
            MainScreen(auth, newsViewModel, newPostViewModel, homeViewModel, context, list.value)
        }
    }

    override fun onResume() {
        super.onResume()
        exampleLiveData.value = DateTimeFormatter.ISO_INSTANT.format(Instant.now())
    }

}

@ExperimentalComposeUiApi
@Composable
fun MainScreen(
    auth: FirebaseAuth,
    newsViewModel: NewsViewModel,
    newPostViewModel: NewPostViewModel,
    homeViewModel: HomeViewModel,
    context: Activity,
    list: MutableList<Post>
) {
    val navController: NavHostController = rememberNavController()
    Scaffold(
        topBar = { TopBar(context) },
        bottomBar = { BottomNavigationBar(navController) }
    ) {
        NavigationBottom(
            navController,
            auth,
            newsViewModel,
            newPostViewModel,
            homeViewModel,
            context,
            list
        )
    }
}