package com.example.app.ui.components

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.app.data.model.Post
import com.example.app.ui.home.HomeScreen
import com.example.app.ui.home.HomeViewModel
import com.example.app.ui.newpost.NewPostScreen
import com.example.app.ui.newpost.NewPostViewModel
import com.example.app.ui.news.NewsScreen
import com.example.app.ui.news.NewsViewModel
import com.google.firebase.auth.FirebaseAuth

@ExperimentalComposeUiApi
@Composable
fun NavigationBottom(
    navController: NavHostController,
    auth: FirebaseAuth,
    newsViewModel: NewsViewModel,
    newPostViewModel: NewPostViewModel,
    homeViewModel: HomeViewModel,
    context: Context,
    list: MutableList<Post>
) {
    NavHost(navController, startDestination = NavigationItem.Home.route) {
        composable(NavigationItem.Home.route) {
            HomeScreen(list, auth, homeViewModel, context)
        }
        composable(NavigationItem.News.route) {
            NewsScreen(newsViewModel)
        }
        composable(NavigationItem.NewPost.route) {
            NewPostScreen(navController, newPostViewModel)
        }
    }
}