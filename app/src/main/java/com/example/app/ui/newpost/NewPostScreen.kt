package com.example.app.ui.newpost

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.app.R
import com.example.app.ui.components.NavigationItem
import com.google.firebase.auth.FirebaseAuth

@Composable
fun NewPostScreen(
    navController: NavHostController,
    newPostViewModel: NewPostViewModel
) {
    val user = FirebaseAuth.getInstance().currentUser
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.white_gray))
            .wrapContentSize(Alignment.TopCenter)
    ) {

        Spacer(Modifier.height(16.dp))
        Text(
            text = "Create new post",
            fontWeight = FontWeight.Bold,
            color = Color.Black,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            fontSize = 20.sp
        )
        Card(
            border = BorderStroke(2.dp, Color.Transparent),
            elevation = 12.dp,
            backgroundColor = colorResource(id = R.color.white_gray),
            shape = RoundedCornerShape(10.dp),
            modifier = Modifier
                .padding(10.dp)
                .height(200.dp)
                .fillMaxWidth()
        ) {
            val textState = newPostViewModel.newPostText.value
            val maxChar = 280
            TextField(
                value = textState,
                onValueChange = { newPostViewModel.onNewPostTextChanged(it.take(maxChar)) },
                label = { Text("What are you thinking?") }
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Button(
            onClick = {
                newPostViewModel.onPublishClick(newPostViewModel.newPostText.value)
                newPostViewModel.onNewPostTextChanged("")
                navController.navigate(NavigationItem.Home.route)
            },
            shape = RoundedCornerShape(30.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.Black,
                contentColor = Color.White
            ),
            modifier = Modifier
                .align(Alignment.End)
                .padding(10.dp)
                .width(110.dp)
                .height(50.dp)
        ) {
            Text(text = "Publish", fontWeight = FontWeight.Bold)
        }
    }
}