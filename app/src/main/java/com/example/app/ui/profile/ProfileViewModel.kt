package com.example.app.ui.profile

import androidx.lifecycle.ViewModel
import com.example.app.data.repository.PostRepository
import com.google.firebase.auth.FirebaseAuth

class ProfileViewModel : ViewModel() {

    val auth: FirebaseAuth = FirebaseAuth.getInstance()

    private var postsRepository = PostRepository()

    fun getPostsForProfile() {
        postsRepository.getAllByUserName(auth.currentUser?.email)
    }

}