package com.example.app.ui.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.app.data.model.Post
import com.example.app.data.repository.PostRepository

class NewsViewModel : ViewModel() {

    private var postRepository: PostRepository = PostRepository()

    fun getAllNewsList(): LiveData<List<Post>> {
        return postRepository.getAllPosts()
    }

    fun getNewsFromApiAndPutInDatabase() {
        postRepository.apiCallAndPutInDatabase()
    }

}