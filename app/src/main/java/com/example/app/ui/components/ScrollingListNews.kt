package com.example.app.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.app.R
import com.example.app.data.model.Post
import com.example.app.utils.formatDate

@Composable
fun ImageListItemNews(message: String, imageUrl: String, data: String, user: String) {
    Card(
        border = BorderStroke(2.dp, Color.Transparent),
        elevation = 10.dp,
        backgroundColor = colorResource(id = R.color.white_gray),
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier.padding(5.dp)
    ) {
        Column(
            modifier = Modifier.padding(5.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxHeight()
                    .wrapContentSize(Alignment.Center)

            ) {
                Image(
                    painter = rememberImagePainter(data = imageUrl),
                    contentDescription = "Image of $user",
                    modifier = Modifier
                        .size(60.dp)
                        .clip(CircleShape)
                        .border(1.dp, Color.White, CircleShape)
                        .background(color = colorResource(id = R.color.light_grey))
                )
                Spacer(Modifier.width(10.dp))
                Text(message, style = MaterialTheme.typography.subtitle1)

            }
            Spacer(Modifier.width(10.dp))
            Text(
                data,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.align(Alignment.End)
            )
        }
    }
}

@Composable
fun ScrollingListNews(list: List<Post>) {
    val listSize = list.size
    val scrollState = rememberLazyListState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
    ) {
        LazyColumn(state = scrollState) {
            items(listSize) {
                ImageListItemNews(
                    list[it].message_content,
                    list[it].user_profile,
                    formatDate(list[it].message_created),
                    list[it].user_name
                )
            }
        }
    }
}