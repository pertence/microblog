package com.example.app.ui.home

import android.content.Context
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import coil.compose.rememberImagePainter
import com.example.app.R
import com.example.app.data.model.Post
import com.example.app.ui.detail.DetailActivity
import com.example.app.ui.profile.ButtonLogout
import com.google.firebase.auth.FirebaseAuth

@ExperimentalComposeUiApi
@Composable
fun HomeScreen(
    list: List<Post>,
    auth: FirebaseAuth,
    homeViewModel: HomeViewModel,
    context: Context
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.93f)
            .background(colorResource(id = R.color.white))
            .wrapContentSize(Alignment.Center)
    ) {

        ScrollingListHome(list = list.asReversed(), homeViewModel, context)
        ButtonLogout(auth)
    }
}

@ExperimentalComposeUiApi
@Composable
fun ScrollingListHome(
    list: List<Post>,
    viewModel: ViewModel,
    context: Context
) {
    val listSize = list.size
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
    ) {
        LazyColumn {
            items(listSize) {
                ItemListHome(list[it], viewModel, context)
            }
        }
    }

}

@ExperimentalComposeUiApi
@Composable
fun ItemListHome(
    post: Post,
    viewModel: ViewModel,
    context: Context
) {
    val homeViewModel = viewModel as HomeViewModel
    Card(
        border = BorderStroke(2.dp, Color.Transparent),
        elevation = 12.dp,
        backgroundColor = colorResource(id = R.color.white_gray),
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier
            .padding(6.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .padding(6.dp)
                .clickable {
                    context.startActivity(DetailActivity.newIntent(context, post))
                    homeViewModel.updateIdState(post.id.toInt())

                }
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxHeight()
                    .wrapContentSize(Alignment.Center)
            ) {
                Image(
                    painter = rememberImagePainter(
                        data = post.user_profile
                    ),
                    contentDescription = "Image from ${post.user_name}",
                    modifier = Modifier
                        .size(65.dp)
                        .clip(CircleShape)
                        .border(1.dp, Color.White, CircleShape)
                        .background(
                            color = colorResource(id = R.color.light_grey)
                        )
                )
                Spacer(Modifier.width(10.dp))
                Text(post.user_name, style = MaterialTheme.typography.h6)
            }

            Spacer(Modifier.width(10.dp))
            Text(post.message_content, style = MaterialTheme.typography.subtitle1)
            Spacer(Modifier.width(10.dp))
            Text(
                post.message_created,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.align(Alignment.End)
            )
        }
    }
}

