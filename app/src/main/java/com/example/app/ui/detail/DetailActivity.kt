package com.example.app.ui.detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Scaffold
import androidx.lifecycle.ViewModelProvider
import com.example.app.data.model.Post
import com.example.app.ui.components.DetailTopBar
import com.example.app.ui.home.HomeViewModel
import com.example.app.ui.main.MainActivity
import com.google.firebase.auth.FirebaseAuth

class DetailActivity : ComponentActivity() {

    private val auth = FirebaseAuth.getInstance()
    private lateinit var homeViewModel: HomeViewModel

    private val post: Post by lazy {
        intent?.getSerializableExtra(POST_ID) as Post
    }

    companion object {
        private const val POST_ID = "post_id"

        fun newIntent(context: Context, post: Post) =
            Intent(context, DetailActivity::class.java).apply {
                putExtra(POST_ID, post)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        homeViewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(HomeViewModel::class.java)

        setContent {
            Scaffold(topBar = { DetailTopBar(activity = this) }) {
                DetailScreen(post = post, auth = auth, homeViewModel = homeViewModel, this)
            }
        }
    }
}


fun deleteFromDatabase(
    homeViewModel: HomeViewModel,
    post: Post,
    context: Activity
) {
    homeViewModel.deletePostFromDatabase(post)
    goToMainActivity(context)
}

fun goToMainActivity(context: Activity) {
    val intent = Intent(context, MainActivity::class.java)
    intent.flags =
        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    context.startActivity(intent)
}



