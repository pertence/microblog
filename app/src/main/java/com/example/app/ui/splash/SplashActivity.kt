package com.example.app.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.app.R
import com.example.app.data.model.Post
import com.example.app.ui.home.HomeViewModel
import com.example.app.ui.login.LoginActivity
import com.example.app.ui.main.MainActivity
import com.google.firebase.auth.FirebaseAuth

class SplashActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mAuth = FirebaseAuth.getInstance()
        val user = mAuth.currentUser

        homeViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(HomeViewModel::class.java)

        val listPost: MutableList<Post> = mutableListOf()

        homeViewModel.putFakeOnDatabase()
        homeViewModel.getAllPostList().observe(this, { newList ->
            listPost.addAll(newList)
        })

        if (user != null) {
            startHome()
        } else {
            startLogin()
        }

    }

    private fun startHome() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun startLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}

