package com.example.app.ui.newpost

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.data.model.Post
import com.example.app.data.repository.PostRepository
import com.example.app.utils.formatDate
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch
import java.time.LocalDateTime

class NewPostViewModel : ViewModel() {

    private var postRepository = PostRepository()

    fun getAllNewsList(): LiveData<List<Post>> {
        return postRepository.getAllPosts()
    }

    fun getNewsFromApiAndPutInDatabase() {
        postRepository.apiCallAndPutInDatabase()
    }

    val auth: FirebaseAuth = FirebaseAuth.getInstance()

    val newPostText = mutableStateOf(" ")
    private val textToPublish = mutableStateOf(" ")

    fun onNewPostTextChanged(text: String) {
        this.newPostText.value = text
    }

    fun onPublishClick(text: String) {
        this.textToPublish.value = text
        val date = formatDate(LocalDateTime.now().toString())
        val user = auth.currentUser?.displayName
        val picture = "https://cdn.onlinewebfonts.com/svg/download_191958.png"
        viewModelScope.launch {
            postRepository.insertNewPostOnDatabase(
                Post(
                    user_name = user.toString(),
                    user_profile = picture,
                    message_content = text,
                    message_created = date
                )
            )
        }

    }

}