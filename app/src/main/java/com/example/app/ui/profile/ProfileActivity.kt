package com.example.app.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.app.R
import com.example.app.databinding.ActivityProfileBinding
import com.example.app.ui.login.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val userName = "Someone"

        auth = FirebaseAuth.getInstance()
        changeUserName(userName)

        val userNameText = binding.profileUserName
        val userEditName = binding.profileEditNameButton
        val logoutButton = binding.profileButtonLogout
        val userNameContainer = binding.profileNameLayout

        val editUserNameContainer = binding.profileEditNameLayout
        val userEditText = binding.profileEditNameText
        val confirmEditName = binding.profileButtonConfirmEditName
        val cancelEditName = binding.profileButtonCancelEditName

        val imagePicture = binding.profilePicture
        val imageEditPicture = binding.profileEditPicture

        userNameText.text = auth.currentUser?.displayName.toString()

        userEditName.setOnClickListener {
            editUserNameContainer.visibility = View.VISIBLE
            userNameContainer.visibility = View.GONE
            userEditText.text = userNameText.editableText
        }

        cancelEditName.setOnClickListener {
            editUserNameContainer.visibility = View.GONE
            userNameContainer.visibility = View.VISIBLE
            userEditText.text = null

        }

        confirmEditName.setOnClickListener {
            editUserNameContainer.visibility = View.GONE
            userNameContainer.visibility = View.VISIBLE
            changeUserName(userEditText.text.toString())
            userNameText.text = auth.currentUser?.displayName
        }
    }

    private fun changeUserName(userName: String) {
        val user = auth.currentUser
        user?.updateProfile(
            UserProfileChangeRequest.Builder().setDisplayName(userName).build()
        )
    }
}

@Composable
fun ProfileName(auth: FirebaseAuth) {
    Column {
        Row(modifier = Modifier.fillMaxWidth()) {
            Text(text = auth.currentUser?.displayName!!)
            Icon(
                imageVector = Icons.Default.Edit,
                contentDescription = "Edit name",
                modifier = Modifier.padding(start = 4.dp)
            )
        }

    }
}

@Composable
fun ProfileHeader() {
    Column(
        modifier = Modifier
            .fillMaxWidth(0.8f)
            .fillMaxHeight(0.8f),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = rememberImagePainter(
                data = "https://cdn.onlinewebfonts.com/svg/download_191958.png"
            ),
            contentDescription = "Profile Image",
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .clip(CircleShape)
                .border(2.dp, colorResource(R.color.dark_theme_grey), CircleShape)
                .background(color = colorResource(id = R.color.light_grey))
                .align(Alignment.CenterHorizontally)
        )
    }
}


@Composable
fun ButtonLogout(auth: FirebaseAuth) {
    val context = LocalContext.current

    OutlinedButton(
        onClick = {
            Toast.makeText(context, "Exiting", Toast.LENGTH_SHORT).show()
            auth.signOut()
            context.startActivity(Intent(context, LoginActivity::class.java))
        },
        modifier = Modifier.background(Color.Transparent)
    ) {
        Text(text = "Logout", color = Color.Black)
    }
}