package com.example.app.ui.home

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.data.model.Post
import com.example.app.data.repository.PostRepository
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private var postsRepository = PostRepository()

    private val idPostDetails: MutableState<Int> = mutableStateOf(0)

    val postTextEdit = mutableStateOf(" ")

    fun onPostTextChanged(text: String) {
        this.postTextEdit.value = text
    }


    fun getAllPostList(): LiveData<List<Post>> {
        return postsRepository.getAllPosts()
    }

    fun putFakeOnDatabase() {
        viewModelScope.launch {
            postsRepository.insertFakePostsOnDatabase()
        }
    }

    fun deletePostFromDatabase(post: Post) {
        viewModelScope.launch {
            postsRepository.deletePostFromDatabase(post)
        }
    }

    fun updatePostFromDatabase(post: Post) {
        viewModelScope.launch {
            postsRepository.updatePostOnDatabase(post)
        }
    }

    fun getPostById(id: Int): LiveData<Post> {
        return postsRepository.getPostById(id.toLong())
    }

    fun getIdState(): Int {
        return idPostDetails.value
    }

    fun updateIdState(id: Int) {
        idPostDetails.value = id
    }
}