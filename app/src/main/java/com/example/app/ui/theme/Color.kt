package com.example.app.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFF000000)
val Purple500 = Color(0xFF111713)
val Purple700 = Color(0xFF000000)
val Teal200 = Color(0x66000000)