package com.example.app.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.app.data.model.Post

@Dao
interface PostsDao {

    @Query("SELECT * FROM Posts")
    fun getAllPosts(): LiveData<List<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPost(post: Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllPosts(newsList: List<Post>)

    @Query("DELETE FROM Posts")
    suspend fun deleteAllPost()

    @Query("DELETE FROM Posts WHERE id = :postId")
    suspend fun deleteByPostId(postId: Long)

    @Query("SELECT * FROM Posts where id = :postId")
    fun getPostById(postId: Long): LiveData<Post>

    @Update
    suspend fun updatePost(post: Post)

    @Query("SELECT * FROM Posts WHERE user_name = :user")
    fun getPostsByUserName(user: String?): LiveData<List<Post>>

}