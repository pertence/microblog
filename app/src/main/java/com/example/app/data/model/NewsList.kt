package com.example.app.data.model

data class NewsList(
    val news: List<News>
)