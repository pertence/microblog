package com.example.app.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.app.MicroBlogApplication
import com.example.app.data.model.News
import com.example.app.data.model.NewsList
import com.example.app.data.model.Post
import com.example.app.data.returnListOfPostsFake
import com.example.app.data.service.RestApi
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PostRepository {

    private val dao = MicroBlogApplication.appDatabase!!.postDao()

    fun getAllPosts(): LiveData<List<Post>> = dao.getAllPosts()

    fun getPostById(postId: Long) = dao.getPostById(postId)

    suspend fun insertFakePostsOnDatabase() = dao.insertAllPosts(returnListOfPostsFake())

    suspend fun insertNewPostOnDatabase(post: Post) = dao.insertPost(post)

    suspend fun deletePostFromDatabase(post: Post) = dao.deleteByPostId(post.id)

    suspend fun updatePostOnDatabase(post: Post) = dao.updatePost(post)

    fun getAllByUserName(user: String?) = dao.getPostsByUserName(user)

    fun apiCallAndPutInDatabase() {
        val gson = Gson()
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl("https://gb-mobile-app-teste.s3.amazonaws.com/")
            .build()

        val restApi = retrofit.create(RestApi::class.java)

        restApi.getResponse().enqueue(object : Callback<NewsList?> {
            override fun onResponse(
                call: Call<NewsList?>,
                newsList: retrofit2.Response<NewsList?>
            ) {
                Log.d("DEV", newsList.body().toString())
                when (newsList.code()) {
                    200 -> {
                        CoroutineScope(Dispatchers.IO).launch {
                            val listNewsResponse: List<News> = newsList.body()!!.news
                            val listNewsResult: List<Post> = formatToDatabase(listNewsResponse)
                            dao.deleteAllPost()
                            dao.insertAllPosts(listNewsResult)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<NewsList?>, t: Throwable) {
                Log.d("DEV", "Something went wrong")
            }
        })
    }

    fun formatToDatabase(listNews: List<News>): List<Post> {

        val resultList: MutableList<Post> = mutableListOf()

        for (new in listNews) {
            val userName = new.user.name
            val userPicture = new.user.profile_picture
            val messageContent = new.message.content
            val messageCreated = new.message.created_at
            val newsModel = Post(
                user_name = userName,
                user_profile = userPicture,
                message_content = messageContent,
                message_created = messageCreated
            )
            resultList.add(newsModel)
        }

        return resultList
    }

}