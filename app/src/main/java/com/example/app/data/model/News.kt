package com.example.app.data.model

data class News(
    val message: Message,
    val user: User
)