package com.example.app.data.service

import com.example.app.data.model.NewsList
import retrofit2.Call
import retrofit2.http.GET

interface RestApi {

    @GET("data.json")
    fun getResponse(): Call<NewsList>

}