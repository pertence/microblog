package com.example.app.data

import com.example.app.data.model.Post

fun returnListOfPostsFake(): List<Post> {
    val resultList = mutableListOf<Post>()

    resultList.add(
        Post(
            1,
            "User Name 1",
            "https://randomuser.me/api/portraits/med/women/3.jpg",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "12h51 - 20/07/2021"
        )
    )

    resultList.add(
        Post(
            2,
            "User Name 2",
            "https://randomuser.me/api/portraits/med/women/5.jpg",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "20h10 - 01/07/2019"
        )
    )

    resultList.add(
        Post(
            3,
            "User Name 3",
            "https://randomuser.me/api/portraits/med/men/7.jpg",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "08h42 - 21/07/2021"
        )
    )
    return resultList
}