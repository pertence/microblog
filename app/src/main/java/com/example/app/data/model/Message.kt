package com.example.app.data.model

data class Message(
    val content: String,
    val created_at: String
)