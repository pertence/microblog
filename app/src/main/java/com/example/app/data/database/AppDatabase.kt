package com.example.app.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.app.data.model.Post

@Database(entities = [(Post::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostsDao
}