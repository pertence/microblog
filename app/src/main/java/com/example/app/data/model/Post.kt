package com.example.app.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Posts")
data class Post(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var user_name: String,
    var user_profile: String,
    var message_content: String,
    var message_created: String
) : Serializable


