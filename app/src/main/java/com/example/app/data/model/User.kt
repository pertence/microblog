package com.example.app.data.model

data class User(
    val name: String,
    val profile_picture: String
)